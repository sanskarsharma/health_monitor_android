package dev.sanskar.com.healthmonitor;

/**
 * Created by Sanskar on 01-May-17.
 */
public class DataModel {

    int upper,lower,pulse,sugar;
    String sugarTime,  note, createdAt;


    public DataModel(int upper, int lower, int pulse, int sugar,String sugarTime, String note, String createdAt) {
        this.upper = upper;
        this.lower = lower;
        this.pulse = pulse;
        this.sugar = sugar;
        this.note = note;
        this.createdAt = createdAt;
        this.sugarTime = sugarTime;
    }

    public String getSugarTime() {
        return sugarTime;
    }

    public void setSugarTime(String sugarTime) {
        this.sugarTime = sugarTime;
    }

    public DataModel() {
    }

    public int getUpper() {
        return upper;
    }

    public void setUpper(int upper) {
        this.upper = upper;
    }

    public int getLower() {
        return lower;
    }

    public void setLower(int lower) {
        this.lower = lower;
    }

    public int getPulse() {
        return pulse;
    }

    public void setPulse(int pulse) {
        this.pulse = pulse;
    }

    public int getSugar() {
        return sugar;
    }

    public void setSugar(int sugar) {
        this.sugar = sugar;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
