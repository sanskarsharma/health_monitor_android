package dev.sanskar.com.healthmonitor;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Sanskar on 01-May-17.
 */
public class DataAdapter extends RecyclerView.Adapter<DataAdapter.MyViewHolder> {

    private List<DataModel> dataList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, date, bp, note,time;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.sugarView);
            bp = (TextView) view.findViewById(R.id.bp);
            date = (TextView) view.findViewById(R.id.timestamp);
            note = (TextView)view.findViewById(R.id.notekiid);
            time = (TextView)view.findViewById(R.id.timekiid);

        }
    }

    public DataAdapter() {
    }

    public DataAdapter(List<DataModel> dataList) {
        this.dataList = dataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DataModel datu = dataList.get(position);

        String[] arr =getDateandtime(Long.parseLong(datu.getCreatedAt())).split("-");
        String entryDate= arr[0];
        String entryTime= arr[1];

        holder.note.setText(datu.getNote());
        holder.title.setText("Blood Sugar : " + datu.getSugar()+" ( "+datu.getSugarTime()+" )");
        holder.bp.setText("BP : "+datu.getUpper()+"/"+datu.getLower()+" mmHg "+"\n"+"with PULSE rate : "+datu.getPulse()+"beats/min");
        holder.date.setText(entryDate);
        holder.time.setText(entryTime);


    }
    public  String getDateandtime(long timestamp) {
        try{
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getTimeZone("India/Kolkata");
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdfDate = new SimpleDateFormat("EEE, MMM d, ''yy");
            SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a");

            java.util.Date currenTimeZone = (Date) calendar.getTime();
            return sdfDate.format(currenTimeZone)+"-"+sdfTime.format(currenTimeZone);
        }catch (Exception e) {
        }
        return "";
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void refreshList(List<DataModel> dataList){

        this.dataList = dataList;
        notifyItemInserted(dataList.size() - 1);

    }


}