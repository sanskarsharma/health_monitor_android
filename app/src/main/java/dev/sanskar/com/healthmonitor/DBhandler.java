package dev.sanskar.com.healthmonitor;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sanskar on 01-May-17.
 */
public class DBhandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 3;

    // Database Name
    private static final String DATABASE_NAME = "dataDB";

    // Notes table name
    private static final String TABLE_DATA = "datatable1";

    // Notes Table Columns names
    private static final String KEY_NOTE = "note";
    private static final String KEY_CREATEDAT = "createdAt";
    private static final String KEY_UPPER = "upperBP";
    private static final String KEY_LOWER = "lowerBP";
    private static final String KEY_PULSE = "pulse";
    private static final String KEY_SUGAR = "sugar";
    private static final String KEY_SUGAR_TIME = "sugar_time";


    public DBhandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_DATA_TABLE = "CREATE TABLE " + TABLE_DATA + "("
                + KEY_CREATEDAT + " TEXT PRIMARY KEY," + KEY_NOTE + " TEXT, "+KEY_UPPER+" INTEGER, "+ KEY_LOWER+" INTEGER, "+ KEY_PULSE+" INTEGER, "+KEY_SUGAR+" INTEGER, "+KEY_SUGAR_TIME+" TEXT "+")";
        db.execSQL(CREATE_DATA_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DATA);

        // Create tables again
        onCreate(db);
    }



    public void addData(DataModel dataModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_CREATEDAT, dataModel.getCreatedAt()); // Note is created at
        values.put(KEY_NOTE, dataModel.getNote()); // Note data
        values.put(KEY_UPPER, dataModel.getUpper()); // Note created by
        values.put(KEY_LOWER, dataModel.getLower());
        values.put(KEY_PULSE, dataModel.getPulse());
        values.put(KEY_SUGAR, dataModel.getSugar());
        values.put(KEY_SUGAR_TIME, dataModel.getSugarTime());

        // Inserting Row
        db.insert(TABLE_DATA, null, values);
        db.close(); // Closing database connection
    }

    // Getting All Notes of a User
    public List<DataModel> getAllEntries() {
        List<DataModel> datalist = new ArrayList<DataModel>();

        String selectQuery = "SELECT  * FROM " + TABLE_DATA ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DataModel datu = new DataModel();
                datu.setCreatedAt(cursor.getString(0));
                datu.setNote(cursor.getString(1));
                datu.setUpper(cursor.getInt(2));
                datu.setLower(cursor.getInt(3));
                datu.setPulse(cursor.getInt(4));
                datu.setSugar(cursor.getInt(5));
                datu.setSugarTime(cursor.getString(6));




                // Adding note to list
                datalist.add(datu);
            } while (cursor.moveToNext());
        }

        // return notes list
        return datalist;
    }

    // Deleting note
    public void deleteData(DataModel dataModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_DATA, KEY_CREATEDAT + " = ?",
                new String[]{String.valueOf(dataModel.getCreatedAt())});
        db.close();
    }

    // Updating note
    public int updateData(DataModel dataModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CREATEDAT, dataModel.getCreatedAt()); // Note is created at
        values.put(KEY_NOTE, dataModel.getNote()); // Note data
        values.put(KEY_UPPER, dataModel.getUpper()); // Note created by
        values.put(KEY_LOWER, dataModel.getLower());
        values.put(KEY_PULSE, dataModel.getPulse());
        values.put(KEY_SUGAR, dataModel.getSugar());
        values.put(KEY_SUGAR_TIME, dataModel.getSugarTime());


        // updating row
        return db.update(TABLE_DATA, values, KEY_CREATEDAT + " = ?",
                new String[] { String.valueOf(dataModel.getCreatedAt()) });
    }



    // Getting notes Count
    // unused function
    public int getEntryCount() {
        String countQuery = "SELECT  * FROM " + TABLE_DATA ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        return cursor.getCount();
    }

}
