package dev.sanskar.com.healthmonitor;

import android.content.DialogInterface;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                // Get the layout inflater
                LayoutInflater inflater = MainActivity.this.getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                final View fullview = inflater.inflate(R.layout.dialogi, null);
                final EditText upper,lower,pulse,sugar,note;
                final Spinner sugarSpinner;
                upper = (EditText)fullview.findViewById(R.id.enterupper);
                lower = (EditText)fullview.findViewById(R.id.enterlower);
                pulse=(EditText)fullview.findViewById(R.id.enterpulse);
                sugar=(EditText)fullview.findViewById(R.id.entersugar);
                note= (EditText)fullview.findViewById(R.id.enternote);
                sugarSpinner=(Spinner)fullview.findViewById(R.id.sugarSpinner);

                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                        R.array.sugar_time_choices, android.R.layout.simple_spinner_item);
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                sugarSpinner.setAdapter(adapter);

                sugarSpinner.setAdapter(
                        new Spinner_Initial_Message_Adapter(
                                adapter,
                                R.layout.spinner_initial_message,
                                // R.layout.contact_spinner_nothing_selected_dropdown, // Optional
                                getApplicationContext()));




                builder.setView(fullview)
                        // Add action buttons
                        .setPositiveButton("Add Entry", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {


                                int upperdata,lowerdata,pulsedata,sugardata;

                                if(upper.getText().toString().length()==0)
                                     upperdata = 10101010;
                                 else
                                    upperdata = Integer.parseInt(upper.getText().toString());

                                if(lower.getText().toString().length()==0)
                                    lowerdata = 10101010;
                                else
                                    lowerdata = Integer.parseInt(lower.getText().toString());


                                if(pulse.getText().toString().length()==0)
                                    pulsedata = 10101010;
                                else
                                    pulsedata = Integer.parseInt(pulse.getText().toString());

                                if(sugar.getText().toString().length()==0)
                                    sugardata = 10101010;
                                else
                                    sugardata = Integer.parseInt(sugar.getText().toString());




                                String notedata = note.getText().toString();

                                Long tsLong = System.currentTimeMillis() / 1000;
                                String timestampdata = tsLong.toString();

                                String spinnerChoice = sugarSpinner.getSelectedItem().toString();

                                DBhandler dbh = new DBhandler(getApplicationContext());
                                DataModel datu = new DataModel(upperdata,lowerdata,pulsedata,sugardata,spinnerChoice,notedata,timestampdata);

                                dbh.addData(datu);   // adding new entry to database

                                RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler_view);     // did this to re render the updated recycleview
                                recyclerView.getRecycledViewPool().clear();


                                List<DataModel> items = new DBhandler(getApplicationContext()).getAllEntries(); // getting new list from database for filling in the recycleview
                                Collections.reverse(items);

                                DataAdapter obj = new DataAdapter(); // notifying adapter with refreshed list
                                obj.refreshList(items);

                                recyclerView.setAdapter(obj); // setting adapter to recycleview






                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                builder.show();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            //Returning the current tabs
            switch (position) {
                case 0:
                    Frag_Tab_1 tab1 = new Frag_Tab_1();
                    return tab1;
                case 1:
                    Frag_Tab_2 tab2 = new Frag_Tab_2();
                    return tab2;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "BP/Sugar";
                case 1:
                    return "Insulin Intake";

            }
            return null;
        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (v.getId()==R.id.recycler_view) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.context_menu, menu);
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);



    }



}
