package dev.sanskar.com.healthmonitor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Sanskar on 4/27/2017.
 */
public class Frag_Tab_1 extends Fragment {

    TextView txt;
    static RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_tab_1, container, false);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView=(RecyclerView) getView().findViewById(R.id.recycler_view);
        registerForContextMenu(recyclerView); /// registering for context menu which gives edit and delete options

        List<DataModel> items = null;

        DBhandler dbh = new DBhandler(getActivity());

        items= dbh.getAllEntries();



        if(items!=null){
            Collections.reverse(items);             // reversing list so that new entries show on top

            DataAdapter adapter = new DataAdapter(items);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();




        }


    }

}
